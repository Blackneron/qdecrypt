#!/bin/bash

########################################################################
#                                                                      #
#                           Q D E C R Y P T                            #
#                                                                      #
#   D E C R Y P T   Q N A P   H Y B R I D   B A C K U P   F I L E S    #
#                                                                      #
#                      Copyright 2019 by PB-Soft                       #
#                                                                      #
#                             pb-soft.com                              #
#                                                                      #
#                             Version 1.0                              #
#                                                                      #
########################################################################
#                                                                      #
#  Please check that the 'openssl' software packages is installed and  #
#  configured on your system.                                          #
#                                                                      #
########################################################################
#                                                                      #
#  Script usage:                                                       #
#                                                                      #
#   $ ./qdecrypt.sh                                                    #
#                                                                      #
#   => Shows some information about the usage of this script.          #
#                                                                      #
#                                                                      #
#   $ ./qdecrypt.sh "<filename>" <password>"                           #
#                                                                      #
#   => Decrypts the specified file.                                    #
#                                                                      #
#                                                                      #
#   Example:                                                           #
#                                                                      #
#   $ ./qdecrypt.sh "my_file.txt" "My_sEcReT_PaSSwORD"                 #
#                                                                      #
#   => Decrypts the file "my_file.txt" with the specified password     #
#      "My_sEcReT_PaSSwORD" to the file "decrypted_my_file.txt".       #
#                                                                      #
########################################################################


# ======================================================================
#            U S E R   C O N F I G U R A T I O N   B E G I N
# ======================================================================

# Specify the message digest type.
# - Older versions of openssl/NAS use: md5
# - Newer versions of openssl/NAS use: sha256
DIGEST="md5"


# ======================================================================
#            U S E R   C O N F I G U R A T I O N   E N D
# ======================================================================

# Specify the script version.
VERSION="1.0"

# Get the filename from the user input.
FILENAME=$1

# Get the password from the user input.
PASSWORD=$2

# Check if the user input was not correct.
if [[ -z $2 ]]; then

    # Display an information/help message.
    echo
    echo "QDECRYPT - Version: "$VERSION
    echo
    echo "Qdecrypt is used to decrypt one file from a QNAP Hybrid backup (openssl)."
    echo
    echo "Usage: qdecrypt \"<filename>\" \"<password>\""
    echo
    echo "Copyright 2019 by PB-Soft"
    echo

# Both input strings are available.
else

    # Some debug information.
    # echo "Path and filename: "$FILENAME
    # echo "Only filename: "${FILENAME##*/}
    # echo "Only path: "${FILENAME%/*}

    # Check if the input file does exist.
    if [ -f "$FILENAME" ]; then

        # Decrypt the file and save it with the 'decrypted' string.
        openssl enc -md $DIGEST -d -aes-256-cbc -k $PASSWORD -in $FILENAME -out ${FILENAME%/*}/decrypted_${FILENAME##*/}

    # The file does not exist.
    else

        # Display an error message.
        echo "The file '$FILENAME' does not exist!"
    fi
fi
