#!/bin/bash

########################################################################
#                                                                      #
#                           Q D E C R Y P T                            #
#                                                                      #
#   D E C R Y P T   Q N A P   H Y B R I D   B A C K U P   F I L E S    #
#                                                                      #
#                      Copyright 2019 by PB-Soft                       #
#                                                                      #
#                             pb-soft.com                              #
#                                                                      #
#                             Version 1.0                              #
#                                                                      #
########################################################################
#                                                                      #
#  Please check that the 'openssl' software packages is installed and  #
#  configured on your system.                                          #
#                                                                      #
########################################################################
#                                                                      #
#  Script usage:                                                       #
#                                                                      #
#   $ ./qdecrypt_dir.sh                                                #
#                                                                      #
#   => Shows some information about the usage of this script.          #
#                                                                      #
#                                                                      #
#   $ ./qdecrypt_dir.sh "<input_dir>" "<output_dir>" "<password>"      #
#                                                                      #
#   => Decrypts all files from the input folder to the output folder.  #
#                                                                      #
#                                                                      #
#   Example:                                                           #
#                                                                      #
#   $ ./qdecrypt_dir.sh "backup/" "decrypted/" "My_sEcReT_PaSSwORD"    #
#                                                                      #
#   => Decrypts all files from the input folder "backup/" to the       #
#      folder "decrypted/" with the password "My_sEcReT_PaSSwORD".     #
#                                                                      #
########################################################################


# ======================================================================
#            U S E R   C O N F I G U R A T I O N   B E G I N
# ======================================================================

# Specify the message digest type.
# - Older versions of openssl/NAS use: md5
# - Newer versions of openssl/NAS use: sha256
DIGEST="md5"


# ======================================================================
#            U S E R   C O N F I G U R A T I O N   E N D
# ======================================================================

# Specify the script version.
VERSION="1.0"

# Get the input directory from the user input.
INPUT_DIR=$1

# Get the output directory from the user input.
OUTPUT_DIR=$2

# Get the password from the user input.
PASSWORD=$3

# Check if the user input was not correct.
if [[ -z $3 ]]; then

    # Display an information/help message.
    echo
    echo "QDECRYPT_DIR - Version: "$VERSION
    echo
    echo "Qdecrypt_dir is used to decrypt files from a QNAP Hybrid backup (openssl)."
    echo
    echo "Usage: qdecrypt_dir \"<input_dir>\" \"<output_dir>\" \"<password>\""
    echo
    echo "Copyright 2019 by PB-Soft"
    echo

# All the input strings are available.
else

    # Loop through all the files in the directory (recursively) - Begin.
    for item in $(find "$INPUT_DIR"); do

        # Check if the actual item is a directory.
        if [ -d "$item" ]; then

            # Specify the new output directory.
            NEW_DIR="${item/$INPUT_DIR/$OUTPUT_DIR}"

            # Check if the new directory does not already exist.
            if [ ! -d "$NEW_DIR" ]; then

                # Display an information message.
                echo "Creating directory: "$NEW_DIR

                # Make the new directory.
                mkdir $NEW_DIR
            fi

        # The actual item is a file.
        else

            # Specify the new output file.
            NEW_FILE="${item/$INPUT_DIR/$OUTPUT_DIR}"

            # Check if the new file does not already exist.
            if [ ! -f "$NEW_FILE" ]; then

                # Display an information message.
                echo "Decrypting the file '"$item"..."

                # Decrypt the file and save it.
                openssl enc -md $DIGEST -d -aes-256-cbc -k $PASSWORD -in $item -out $NEW_FILE
            fi
        fi
    done
fi
