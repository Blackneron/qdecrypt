# Qdecrypt - README #
---

### Overview ###

The **Qdecrypt** scripts (Bash) can be used to decrypt files from a **client side encrypted** backup of the [**QNAP Hybrid Backup Sync Software**](https://www.qnap.com/solution/hybrid-backup-sync/en-us/). The script **qdecrypt.sh** will decrypt only one file and the script **qdecrypt_dir.sh** will decrypt a whole folder recursively. In the configuration section of the scripts you can specify the used message digest algorithm (**md5** or **sha256**), because depending on the used software version of [**OpenSSL**](https://www.openssl.org/) and the specified default algorithm, this may vary.

### Setup ###

* Upload the two scripts **qdecrypt.sh** and **qdecrypt_dir.sh** to your host.
* Customize the configuration section at the beginning of the files (optional).
* Run one of the two scripts without parameters to see the usage instructions.
* Run the scripts with the necessary parameters to decrypt files/folders.

### Support ###

This is a free script and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **Qdecrypt** script is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](http://opensource.org/licenses/MIT).
